from django import forms
from models import Developer


class DeveloperForm(forms.ModelForm):
    first_name = forms.CharField(required=True)
    last_name = forms.CharField(required=True)

    class Meta:
        model = Developer
        fields = ('username', 'first_name', 'last_name', 'email', 'show_email', 'website', 'twitter_handle', 'interests', 'project', 'available', 'location', 'pict')
